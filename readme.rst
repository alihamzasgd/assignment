###################
Order Management System
###################

Following is an order management system developed in Codeigniter that is 
an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries.

**************************
Development Technology Stack
**************************
- CodeIgniter (PHP Framework)
- MySQL
- JavaScript/JQuery

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Clone and Plcae the repository inside the web server "www" folder. 
1- In case of wamp server, placed cloned code under "wamp/www".
2- In case of xampp server, plance cloned core under "xampp/htdocs".


*******************
Database Migrations
*******************
First of all, create an empty database (MySql). Update database details ( e.g. db name, username, password) in database file under application/config directory.

Then navigate to the project root folder and run below command to run database Migrations.

php index.php migrate

Incase of success, you will receieve following message "Migrations ran successfully!". Now you must be able to access the application on link below
http://localhost/assignment/order 

Make sure that PHP path is added to the environment variable.


*********
Resources
*********

-  `User Guide <https://codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community Slack Channel <https://codeigniterchat.slack.com>`_