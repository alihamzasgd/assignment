<?php


class OrderModel extends CI_Model{


    /**
     * function to get list of all orders in the database
     */
    public function get_order($startDate = '' ,$endDate = ''){

        $sub_query = "";
        if($startDate != ''){
            $sub_query = " AND date(odr.created_at) >= '".$startDate."' AND date(odr.created_at) <= '".$endDate."' ";
        }
        $sql = "SELECT odr.id,
                      user.full_name,
                      product.name,
                      product.price,
                      odr.quantity,
                      odr.total_bill, 
                      odr.created_at 
                    FROM `order` odr, 
                      product,
                      user 
                    WHERE 
                      odr.user_id = user.id
                      AND odr.product_id = product.id $sub_query ORDER by odr.created_at desc";
        $query = $this->db->query($sql);

        $data = array(); 

        if($query->num_rows() > 0 ){
            foreach($query->result() as $row){
                $temp = array();
                
                $temp["id"] = $row->id;
                $temp["full_name"] = $row->full_name;
                $temp["name"] = $row->name;
                $temp["price"] = $row->price;
                $temp["quantity"] = $row->quantity;
                $temp["total_bill"] = $row->total_bill;

                ### timestamp convertion to formated date and time ###
                $time = date("h.i A", strtotime($row->created_at));
                $date = date("d M Y", strtotime($row->created_at));
                $temp["created_at"] = $date." ".$time;

                $data[] = $temp;
                
            }
            return json_encode(array("status"=> 1, "results" => $data));
        }else{
            return json_encode(array("status"=> 1, "results" => $data));
        }
    }


    /**
     * function to create a new order
     */
    public function insert_order($user_id, $product_id, $quantity)
    {    

        $product = $this->get_products($product_id)[0];

        ### selected product price and discount details ###
        $price = $product["price"];
        $discountLimit = $product["discount_limit"];
        $discountPercent = $product["discount_percent"];

        ### discounted price calculation 
        $discountedPrice = calculateDiscountedPrice($price, $discountLimit, $discountPercent,$quantity);
        

        ### compilation of data to be inserted ###
        $insertData["user_id"] = $user_id;
        $insertData["product_id"] = $product_id;
        $insertData["quantity"] = $quantity;
        $insertData["total_bill"] = $discountedPrice;

        $this->db->insert('order', $insertData);
        return true;
    }


    /**
     * function to update an existing order
     */
    public function update_order($order_id, $user_id, $product_id, $quantity) 
    {
        $product = $this->get_products($product_id)[0];
        
        ### selected product price and discount details ###
        $price = $product["price"];
        $discountLimit = $product["discount_limit"];
        $discountPercent = $product["discount_percent"];

        ### discounted price calculation 
        $discountedPrice = calculateDiscountedPrice($price, $discountLimit, $discountPercent, $quantity);

        ### compilation of data to be updated ###
        $updateData["user_id"] = $user_id;
        $updateData["product_id"] = $product_id;
        $updateData["quantity"] = $quantity;
        $updateData["total_bill"] = $discountedPrice;

        $this->db->where('id',$order_id);
        $this->db->update('order', $updateData);
        return true;
    }

    /**
     * function to find an order and its details
     */
    public function find_order($id)
    {
        $sql = "SELECT odr.id,
                      user.full_name,
                      product.name,
                      product.price,
                      odr.quantity,
                      odr.total_bill, 
                      odr.created_at,
                      odr.user_id,
                      odr.product_id 
                    FROM `order` odr, 
                      product,
                      user 
                    WHERE 
                      odr.user_id = user.id
                      AND odr.product_id = product.id
                      AND odr.id = $id";
        $query = $this->db->query($sql);

        $data = array(); 

        if($query->num_rows() > 0 ){
            foreach($query->result() as $row){
                $temp = array();
                
                $temp["id"] = $row->id;
                $temp["user_id"] = $row->user_id;
                $temp["product_id"] = $row->product_id;
                $temp["full_name"] = $row->full_name;
                $temp["name"] = $row->name;
                $temp["price"] = $row->price;
                $temp["quantity"] = $row->quantity;
                $temp["total_bill"] = $row->total_bill;

                $time = date("h.i A", strtotime($row->created_at));
                $date = date("d-m-Y", strtotime($row->created_at));
                $temp["created_at"] = $date." ".$time;

                $data[] = $temp;
                
            }
            return $data;
        }else{
            return array();
        }
    }


    /**
     * function to delete an order
     */
    public function delete_order($id)
    {
        ### deletion of the specified order ###
        return $this->db->delete('order', array('id' => $id));
    }


    /**
     * Function to get list of all users in the database
     */
    public function get_users($id = ''){

        $sub_query = '';
        if($id != ''){
            $sub_query = " AND id = $id ";
        }
        $sql = "SELECT id,full_name FROM user where 1=1 $sub_query ";
        $query = $this->db->query($sql);
        $data = array(); 

        if($query->num_rows() > 0 ){
            foreach($query->result() as $row){
                $temp = array();
                
                $temp["id"] = $row->id;
                $temp["full_name"] = $row->full_name;
                
                $data[] = $temp;
                
            }
            return $data;
        }else{
            return array();
        }
    }

    
    /**
     * Function to get list of all products in the database
     */
    public function get_products( $id=''){

        $sub_query = '';
        if($id != ''){
            $sub_query = " AND id = $id ";
        }

        $sql = "SELECT id, name, price, discount_limit, discount_percent FROM product where 1=1 $sub_query";
        $query = $this->db->query($sql);
        $data = array(); 

        if($query->num_rows() > 0 ){
            foreach($query->result() as $row){
                $temp = array();
                
                $temp["id"] = $row->id;
                $temp["name"] = $row->name;
                $temp["price"] = $row->price;
                $temp["discount_limit"] = $row->discount_limit;
                $temp["discount_percent"] = $row->discount_percent;
                
                $data[] = $temp;
                
            }
            return $data;
        }else{
            return array();
        }
    }
}
?>