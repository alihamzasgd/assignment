<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tables extends CI_Migration {

        public function up()
        {
        
                ### user table creation ###
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'full_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('user');

                ### user table data insertion ###
                $sqlUser = "INSERT INTO `user` (`id`, `full_name`) VALUES
                (1, 'Ali Hamza'),
                (2, 'Hammad'),
                (3, 'Saad'),
                (4, 'David Keith'),
                (5, 'Paul Walker'),
                (6, 'Cristiano Ronaldo'),
                (7, 'Leo Messi');";
                $this->db->query($sqlUser);


                ### product table creation ###
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'price' => array(
                                'type' => 'FLOAT',
                                'constraint' => 11,
                                'null' => TRUE,
                                'default'=> 0
                        ),
                        'discount_limit' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                                'default'=> 0
                        ),
                        'discount_percent' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                                'default'=> 0
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('product');

                ### product table data insertion ###
                $sqlProduct = "INSERT INTO `product` (`id`, `name`, `price`, `discount_limit`, `discount_percent`) VALUES
                (1, 'Pepsi Cola', 1.6, 3, 20),
                (2, 'Coca Cola', 1.8, 0, 0),
                (3, 'Fanta', 1.5, 0, 0);";
                $this->db->query($sqlProduct);


                ### order table creation ###
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                        ),
                        'product_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                        ),
                        'quantity' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                        ),
                        'total_bill' => array(
                                'type' => 'FLOAT',
                                'constraint' => 11,
                                'null' => TRUE,
                                'default'=> 0
                        ),
                ));
                $this->dbforge->add_field(" `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP");
                $this->dbforge->add_field(" `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP");
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('order');

                ### order table data insertion ###
                $sqlOrder = "INSERT INTO `order` (`id`, `user_id`, `product_id`, `quantity`, `total_bill`, `created_at`, `updated_at`) VALUES
                (12, 3, 2, 1, 1.8, '2019-09-26 21:16:17', '2019-09-26 21:16:17'),
                (13, 6, 2, 1, 1.8, '2019-09-26 21:16:21', '2019-09-26 21:16:21'),
                (10, 7, 2, 5, 9, '2019-09-26 21:04:59', '2019-09-26 23:41:58'),
                (5, 1, 1, 3, 3.84, '2019-09-25 22:44:12', '2019-09-25 22:44:12'),
                (19, 2, 1, 5, 6.4, '2019-09-26 21:26:35', '2019-09-26 21:26:35'),
                (7, 1, 1, 1, 1.6, '2019-09-25 22:51:27', '2019-09-25 22:51:27'),
                (8, 7, 1, 3, 3.84, '2019-09-25 22:54:19', '2019-09-26 23:41:38'),
                (14, 4, 3, 1, 1.5, '2019-09-26 21:16:27', '2019-09-26 21:16:27'),
                (15, 2, 3, 1, 1.5, '2019-09-26 21:16:32', '2019-09-26 21:16:32'),
                (16, 1, 1, 1, 1.6, '2019-09-26 21:16:38', '2019-09-26 21:16:38'),
                (18, 3, 3, 1, 1.5, '2019-09-26 21:26:08', '2019-09-26 21:26:08'),
                (20, 4, 3, 33, 49.5, '2019-09-26 23:35:43', '2019-09-26 23:35:43'),
                (21, 4, 2, 12, 21.6, '2019-09-26 23:38:29', '2019-09-26 23:38:29'),
                (22, 1, 1, 12, 15.36, '2019-09-26 23:38:54', '2019-09-26 23:38:54'),
                (23, 1, 1, 12, 15.36, '2019-09-26 23:39:15', '2019-09-26 23:39:15');";
                $this->db->query($sqlOrder);

        }

        public function down()
        {
                $this->dbforge->drop_table('user');
                $this->dbforge->drop_table('product');
                $this->dbforge->drop_table('order');
        }
}

?>