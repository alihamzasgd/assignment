<?php

/**
     * Function to calcute discounted price
     */
    function calculateDiscountedPrice($price, $discountLimit, $discountPercent, $quantity){
        ### discounted price calculation 
        $discountedPrice = $price;
        if($quantity >= $discountLimit){
            $discountedPrice = $price - ( $price * ( $discountPercent / 100) );
        }
        return $discountedPrice * $quantity;
    }