<div class="row">
    <div class="panel panel-success">
        <div class="panel-heading">Edit Order</div>
        <div class="panel-body">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <form method="post" action="#">
                    <?php
                    if ($this->session->flashdata('errors')){
                        echo '<div class="alert alert-danger">';
                        echo $this->session->flashdata('errors');
                        echo "</div>";
                    }
                    ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>User:</strong>
                                <select id="user_id" name="user_id" class="form-control" required>
                                    <option value="" selected disabled>-- select --</option>
                                    <?php
                                    foreach ($users as $user){
                                        $selectedUser = "";
                                        if($user["id"] == $data[0]["user_id"]){
                                            $selectedUser = " selected"; 
                                        }
                                        echo "<option value='".$user["id"]."' $selectedUser >".$user["full_name"]."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>Product:</strong>
                                <select id="product_id" name="product_id" class="form-control" required>
                                    <option value="" selected disabled>-- select --</option>
                                    <?php
                                    foreach ($products as $product){
                                        $selectedProduct = "";
                                        if($product["id"] == $data[0]["product_id"]){
                                            $selectedProduct = " selected";
                                        }
                                        echo "<option value='".$product["id"]."' $selectedProduct >".$product["name"]."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>Quantity:</strong>
                                <input id="quantity" oninput="validity.valid||(value='');" name="quantity" value="<?= $data[0]["quantity"]?>" type="number" min="0" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-primary" href="<?php echo base_url('order');?>"> Back</a>
                            <button type="button" onclick="createNewOrder()" class="btn btn-success"><i class="fa fa-edit"></i> Update Order</button>
                        </div>
                        <input hidden id="order_id" name="order_id" value="<?= $data[0]["id"]?>" type="number">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

// function to add new order on Add order button click
function createNewOrder(){

    // getting form values
    var user_id = $("#user_id").val();
    var product_id = $("#product_id").val();
    var quantity = $("#quantity").val();
    var order_id = $("#order_id").val();

    // sending ajax request to add order
    $.ajax({

        url : "<?= base_url() ?>order/update",
        type : 'POST',
        data : {
            'quantity' : quantity,
            'user_id': user_id,
            'product_id' : product_id,
            'order_id' : order_id
        },
        dataType:'json',
        success : function(res) {       
            // show success alert       
            if(res.status == 1){
                swal({
                    title: "Good Job!",
                    text: 'Order Update Successfully.',
                    icon: "success",
                    button: "Ok",
                });	
            }
        },
        error : function()
        {
            // shoe error in case of failure
            swal({
                title: "Oops!",
                text: 'Could not create order.',
                icon: "error",
                button: "Ok",
            });
        }
    });
}
</script>