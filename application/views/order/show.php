<div class="panel panel-info">
  <div class="panel-heading">Order Details</div>
  <div class="panel-body">
        <div class="row">

            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-user" aria-hidden="true"></i><strong> User:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["full_name"]; ?>
                </div>
            </div>
        
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i><strong> Product:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["name"]; ?>
                </div>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-money" aria-hidden="true"></i> <strong> Price:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["price"]; ?>
                    <i class="fa fa-eur" aria-hidden="true"></i>
                </div>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-calculator" aria-hidden="true"></i><strong> Quantity:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["quantity"]; ?>
                </div>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-money" aria-hidden="true"></i><strong> Total:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["total_bill"]; ?>
                    <i class="fa fa-eur" aria-hidden="true"></i>
                </div>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                <i class="fa fa-calendar" aria-hidden="true"></i><strong> Date:</strong>
                </div>
            </div>
        
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <?php echo $data[0]["created_at"]; ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="pull-left">
                    <a class="btn btn-primary" href="<?php echo base_url('order');?>"> Back</a>
                </div>
            </div>
        
        </div>
    </div>
</div>