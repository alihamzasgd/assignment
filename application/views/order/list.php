
<style>
/* custom css to hide custom range option in daterangepicker */
.ranges li:last-child { 
    display: none; 
}
</style>

<div class="row">
    <div class="jumbotron">
        <h1>Order Management System</h1>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">Create Order</div>
        <div class="panel-body">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <form method="post" action="#">
                    <?php
                    if ($this->session->flashdata('errors')){
                        echo '<div class="alert alert-danger">';
                        echo $this->session->flashdata('errors');
                        echo "</div>";
                    }
                    ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>User:</strong>
                                <select id="user_id" name="user_id" class="form-control" required>
                                    <option value="" selected disabled>-- select --</option>
                                    <?php
                                    foreach ($users as $user){
                                        echo "<option value='".$user["id"]."' >".$user["full_name"]."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>Product:</strong>
                                <select id="product_id" name="product_id" class="form-control" required>
                                    <option value="" selected disabled>-- select --</option>
                                    <?php
                                    foreach ($products as $product){
                                        echo "<option value='".$product["id"]."' >".$product["name"]."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 ">
                            <div class="form-group">
                                <strong>Quantity:</strong>
                                <input id="quantity" oninput="validity.valid||(value='');" name="quantity" type="number" min="0" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="button" onclick="createNewOrder()" class="btn btn-success">Add Order</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="jumbotron">
    
    <div id="reportrange" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 " style="margin-left:30%; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
        <i class="fa fa-calendar"></i>&nbsp;
        <span></span> <i class="fa fa-caret-down"></i>
    </div>
    <br></br>
    <table id="orders_table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>User</th>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Date</th>
            <th width="250px">Action</th>
        </tr>
    </thead>
    <tbody id="ordersTableBody">
    </tbody>
    </table>
</div>

<script>

    // date range picker dates
    var startDate;
    var endDate;
    // function to add new order on Add order button click
    function createNewOrder(){

        // getting form values
        var user_id = $("#user_id").val();
        var product_id = $("#product_id").val();
        var quantity = $("#quantity").val();

        // sending ajax request to add order
        $.ajax({

            url : "<?= base_url() ?>order/store",
            type : 'POST',
            data : {
                'quantity' : quantity,
                'user_id': user_id,
                'product_id' : product_id
            },
            dataType:'json',
            success : function(res) {       
                // show success alert       
                if(res.status == 1){
					swal({
                        title: "Good Job!",
                        text: 'Order Added Successfully.',
                        icon: "success",
                        button: "Ok",
                    });
                    // calling function to update orders
                    getOrders();	
				}
            },
            error : function()
            {
                // shoe error in case of failure
                swal({
                    title: "Oops!",
                    text: 'Could not create order.',
                    icon: "error",
                    button: "Ok",
                });
            }
        });
    }

    // function to add new order on Add order button click
    function getOrders(){

        // getting form values
        startDate = moment(startDate).format('YYYY-MM-DD');
        endDate = moment(endDate).format('YYYY-MM-DD');
        // sending ajax request to add order
        $.ajax({
            url : "<?= base_url() ?>getOrders",
            type : 'POST',
            data : {
                'startDate': startDate,
                'endDate': endDate
            },
            dataType:'json',
            success : function(res) {       
                // show success alert 
                if(res.status == 1){
					dbResults = res.results;
                    var dataTableBody = "";
					dbResults.forEach(function(row) {

                        dataTableBody += ' <tr>'
                            +'<td>'+row.full_name+'</td>'
                            +'<td>'+row.name+'</td>'
                            +'<td>'+row.price+' <i class="fa fa-eur" aria-hidden="true"></i></td> '
                            +'<td>'+row.quantity+'</td>'          
                            +'<td>'+row.total_bill+'<i class="fa fa-eur" aria-hidden="true"></i></td>'
                            +'<td>'+row.created_at+'</td>'+          
                            '<td>'+ '<form method="DELETE" action="<?php echo base_url();?>order/delete/'+row.id+'" >'
                                  +' <a class="btn btn-info btn-sm" href="<?php echo base_url();?>order/'+row.id+'" ><i class="fa fa-eye"></i> show</a>'
                                  +' <a class="btn btn-primary btn-sm" href="<?php echo base_url();?>order/edit/'+row.id+'"><i class="fa fa-edit"></i> Edit</a>'
                                  +' <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>'
                                    +'</form></td></tr>';
					});
                    // redrwaing data table
                    $('#orders_table').DataTable().destroy();
                    $("#ordersTableBody").html(dataTableBody);
                    $('#orders_table').DataTable({
                        "columnDefs": [
                            { "searchable": false, "targets": [2,3,4,5,6] }
                        ]
                    });
				}
            },
            error : function()
            {
                // shoe error in case of failure
                swal({
                    title: "Oops!",
                    text: 'Connection Error! Orders list cannot be loaded.',
                    icon: "error",
                    button: "Ok",
                });
            }
        });
    }

    $(document).ready( function () {
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            startDate = start;
            endDate = end;
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            getOrders();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'All Time': [moment().subtract(2, 'years'), moment()]
            }
        }, cb);
        cb(start, end);

        getOrders();
    });

</script>