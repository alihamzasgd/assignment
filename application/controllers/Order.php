<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends CI_Controller {


   public $order;

   /**
    * Constructor of the class
    *
    * @return Response
   */
   public function __construct() {
      parent::__construct(); 
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->model('OrderModel');
      $this->order = new OrderModel;

      $this->load->helper('common_helper');
   }


   /**
    * Method to list all orders with details
    *
    * @return Response
   */
   public function index()
   {
       $data['users'] = $this->order->get_users();
       $data['products'] = $this->order->get_products();

       $this->load->view('theme/header');       
       $this->load->view('order/list',$data);
       $this->load->view('theme/footer');
   }

   
   /**
    * function to get orders data
    */
    public function get_orders()
    {
        // getting date range to show data
        $startDate = $_POST["startDate"];
        $endDate = $_POST["endDate"];

        // calling model for data retrivel 
        $data =  $this->order->get_order($startDate,$endDate);
        echo $data;    
    }

   
    /**
    * Method to view detials of a perticular order
    *
    * @return Response
   */
   public function show($id)
   {
      $data['data'] = $this->order->find_order($id);
      $this->load->view('theme/header');
      $this->load->view('order/show',$data);
      $this->load->view('theme/footer');
   }


   /**
    * Method to create new order
    *
    * @return Response
   */
   public function create()
   {
      $this->load->view('theme/header');
      $this->load->view('order/create');
      $this->load->view('theme/footer');   
   }


   /**
    * Store Data from this method.
    *
    * @return Response
   */
   public function store()
   {
        $user_id = $_POST["user_id"];
        $product_id = $_POST["product_id"];
        $quantity = $_POST["quantity"];
        $status = $this->order->insert_order($user_id, $product_id, $quantity);
        if($status){
            $data["status"]= 1;
            echo  json_encode($data);
        }else{
            
            $data["status"]= 0;
            echo  json_encode($data);
        }
    }


   /**
    * Method to edit an order.
    *
    * @return Response
   */
   public function edit($id)
    {
        $data['data'] = $this->order->find_order($id);
        $data['users'] = $this->order->get_users();
        $data['products'] = $this->order->get_products();

       $this->load->view('theme/header');
       $this->load->view('order/edit',$data);
       $this->load->view('theme/footer');
   }


   /**
    * Method to update an order.
    *
    * @return Response
   */
   public function update()
   {
        $user_id = $_POST["user_id"];
        $product_id = $_POST["product_id"];
        $quantity = $_POST["quantity"];
        $order_id = $_POST["order_id"];
        $status = $this->order->update_order($order_id, $user_id, $product_id, $quantity);
        if($status){
            $data["status"]= 1;
            echo  json_encode($data);
        }else{
            
            $data["status"]= 0;
            echo  json_encode($data);
        }
   }


   /**
    * Method to delete an order.
    *
    * @return Response
   */
   public function delete($id)
   {
       $item = $this->order->delete_order($id);
       redirect(base_url('order'));
   }
}