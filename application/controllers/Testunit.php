<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Testunit extends CI_Controller {
        private $orderModel = null;
        public function __construct()
        {
            parent::__construct(); 
            $this->load->library("unit_test");
            $this->load->helper('common_helper');	
        }
		
		
		public function index(){
			echo "<h1> Unit Test # 1 </h1>";	
            $test = calculateDiscountedPrice(1.6,3,20,3);
			$expected_result = 3.84;
			$test_name = "Discount Calculation Function (Price = 1.6, Min Item Count for Discount = 3, Discount % = 20, Items Ordered = 3)";
            echo $this->unit->run($test,$expected_result,$test_name);


            echo "<h1> Unit Test # 2 </h1>";	
            $test = calculateDiscountedPrice(2.0,3,20,2);
			$expected_result = 4.0;
			$test_name = "Discount Calculation Function (Price = 2.0, Min Item Count for Discount = 3, Discount % = 20, Items Ordered = 2)";
            echo $this->unit->run($test,$expected_result,$test_name);
            
		}
}